# README #


Files that overwrite files in repos eurofides

### What is this repository for? ###

* Having no writing permissions on the eurofides main repo any modification to it is done here and then imported by composer


### Features ###

* Switch price with or without Italiana TAX dinamicaly
* Update Algolia Search Plug-in with CSS Customization
* BugFix on two Bitbull's modules CRM and CustomBilling
* Added new log element like execution time to CRM log