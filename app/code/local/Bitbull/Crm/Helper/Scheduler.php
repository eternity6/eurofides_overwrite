<?php
/**
 * @category Bitbull
 * @package  Bitbull_Crm
 * @author   Gennaro Vietri <gennaro.vietri@bitbull.it>
 */
class Bitbull_Crm_Helper_Scheduler extends Mage_Core_Helper_Abstract
{
    protected function log($method, $message, $level = Zend_Log::INFO)
    {
        Mage::helper('bitbull_crm')->log($method, $message, $level);
    }

    public function schedulingOrder(Mage_Sales_Model_Order $order)
    {
        try {

            $idCrmAzienda = 0;
            $idCrmIndirizzo = 0;
            $this->log(__METHOD__, "inizio inserimento schedulazione crm data: ".date("d-m-Y H:i:s", time()));
            $customerId = $order->getCustomerId();
            $customer = Mage::getModel('customer/customer')->load($customerId);
            $this->log(__METHOD__, " Order Increment ID: " . $order->getIncrementId()." - Order ID: ".$order->getId());
            $this->log(__METHOD__, " customerOrderId: " . $customerId . " - IdAziendaCrm: " . $customer->getIdAziendaCrm());
            if (!$customer->getIdAziendaCrm()) {
                if (!$customer->getSettoremerceologico() &&
                    !$customer->getComeciconosci() &&
                    !$customer->getTiposocieta()
                ) {
                    Mage::helper('core')->copyFieldset('exportcrm', 'to_customer', Mage::app()->getRequest()->getParam('billing'), $customer);
                }
                $customer->save();
            } else {
                $idCrmAzienda = $customer->getIdAziendaCrm();
            }
            $customerAddress = null;
            if ($order->getShippingAddress()->getCustomerAddressId()) {
                $customerAddress = Mage::getModel('customer/address')->load($order->getShippingAddress()->getCustomerAddressId());
                $this->log(__METHOD__, " customerOrderId: " . $customerId . " - IdIndirizzoCrm se presente: " . $order->getShippingAddress()->getCustomerAddressId());
            } elseif (Mage::helper('bitbull_eurofidescore/address')->IsSameFormalAddress($order->getBillingAddress(), $order->getShippingAddress())) {
                $customerAddress = Mage::getModel('customer/address')->load($order->getBillingAddress()->getCustomerAddressId());
                $this->log(__METHOD__, "shipping address non presente - customerOrderId: " . $customerId . " - IdIndirizzoCrm se presente: " . $order->getBillingAddress()->getCustomerAddressId());
            } else {
                $customerAddress = $order->getShippingAddress();
                $this->log(__METHOD__, "shipping address nuovo - customerOrderId: " . $customerId);
            }
            if ($customerAddress && $customerAddress->getIdIndirizzoCrm()) {
                $idCrmIndirizzo = $customerAddress->getIdIndirizzoCrm();
            }

            $schedulation = Mage::getModel('bitbull_crm/scheduled');
            $schedulation->setOrderId($order->getId());
            $schedulation->setCrmAziendaId($idCrmAzienda);
            $schedulation->setCrmIndirizzoId($idCrmIndirizzo);
            $schedulation->setStatus('pending');
            $schedulation->setRealOrderId($order->getRealOrderId());
            $schedulation->save();

            $this->log(__METHOD__, "fine inserimento schedulazione crm");

        } catch (Exception $e) {
            $this->log(__METHOD__, "Si è verificato un problema con l'ordine", Zend_Log::ERR);
            Mage::logException($e);

            throw new Mage_Exception("Si è verificato un problema con l'ordine.La invitiamo a contattarci ");
        }

    }
}