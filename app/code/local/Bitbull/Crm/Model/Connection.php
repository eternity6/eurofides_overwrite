<?php

/**
 * Created by PhpStorm.
 * User: marco
 * Date: 17/08/15
 * Time: 17.03
 * To change this template use File | Settings | File Templates.
 */
class Bitbull_Crm_Model_Connection extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->setExportUrl(Mage::getStoreConfig('dev/exportorders_crm/export_url'));
        $this->setHelper(Mage::helper('bitbull_crm'));
        if(!$this->sonoAttivo()){
            throw new Mage_Exception(" errore o timeout nella chiamata del WS CRM ");
        }
    }

    protected function log($method, $message, $level = Zend_Log::INFO)
    {
        $this->getHelper()->log($method, $message, $level);
    }

    public function exportAzienda(Mage_Customer_Model_Customer $customer)
    {
        $this->log(__METHOD__, "generazione xml customer");


        if(!$customer->getDefaultBillingAddress()){
            throw new Mage_Core_Exception("Manca il default billing address al cliente ".$customer->getId());
        }

        $this->log(__METHOD__, "generazione xml customer in data: ".date("d-m-Y H:i:s", time()));
        $xmlr = Mage::getModel('bitbull_crm/convert_customer')->xmlCustomer($customer);
        $params = new stdClass();
        $params->pStrXml = str_replace("<?xml version=\"1.0\"?>\n", "", $xmlr->asXML());
        $this->log(__METHOD__, "$params->pStrXml");
        $client = new SoapClient($this->getExportUrl(), array('trace' => 1));
        $result = $client->__soapCall("InserisciAzienda", array($params), null,
            new SoapHeader('http://www.eurofides.com/magento', 'mag'));
        $this->log(__METHOD__, " risposta : " . $client->__getLastResponse());

        if (is_numeric($result->InserisciAziendaResult)) {
            $this->log(__METHOD__, " salvataggio nel customer di id_crm_azienda " . $result->InserisciAziendaResult);

            $customer->setIdAziendaCrm($result->InserisciAziendaResult);
            $customer->save();
            return $result->InserisciAziendaResult;
        }
        return false;

    }

    public function exportIndirizzo($address, $idCrmAzienda)
    {
        $this->log(__METHOD__, "generazione xml address in data: ".date("d-m-Y H:i:s", time()));
        $xmlr = Mage::getModel('bitbull_crm/convert_address')->xmlAddress($address, $idCrmAzienda);
        $params = new stdClass();
        $params->pStrXml = str_replace("<?xml version=\"1.0\"?>\n", "", $xmlr->asXML());
        $this->log(__METHOD__, "$params->pStrXml");
        $client = new SoapClient($this->getExportUrl(), array('trace' => 1));
        $result = $client->__soapCall("InserisciIndirizzo", array($params), null,
            new SoapHeader('http://www.eurofides.com/magento', 'mag'));
        $this->log(__METHOD__, " risposta : " . $client->__getLastResponse());
        if ($result && is_numeric($result->InserisciIndirizzoResult)) {
            $this->log(__METHOD__, " salvataggio nell'address customer di id_crm_indirizzo " . $result->InserisciIndirizzoResult);
            $address->setIdIndirizzoCrm($result->InserisciIndirizzoResult);
            $address->save();
            return $result->InserisciIndirizzoResult;
        }
        return false;

    }

    public function exportOrdine($order, $idCrmAzienda = null, $idCrmIndirizzo = null)
    {
        $this->log(__METHOD__, "generazione xml ordine in data: ".date("d-m-Y H:i:s", time()));
        $customerId = $order->getCustomerId();
        $customer = Mage::getModel('customer/customer')->load($customerId);
        if (!$idCrmAzienda) {
            $idCrmAzienda = $this->exportAzienda($customer);
        }
        $customerAddress = null;
        if (!$idCrmIndirizzo) {
            if ($order->getShippingAddress()->getCustomerAddressId()) {
                $customerAddress = Mage::getModel('customer/address')->load($order->getShippingAddress()->getCustomerAddressId());
                $this->log(__METHOD__, " customerOrderId: " . $customerId . " - IdIndirizzoCrm se presente: " . $order->getShippingAddress()->getCustomerAddressId());
            } elseif (Mage::helper('bitbull_eurofidescore/address')->IsSameFormalAddress($order->getBillingAddress(), $order->getShippingAddress())) {
                $customerAddress = Mage::getModel('customer/address')->load($order->getBillingAddress()->getCustomerAddressId());
                $this->log(__METHOD__, "shipping address non presente - customerOrderId: " . $customerId . " - IdIndirizzoCrm se presente: " . $order->getBillingAddress()->getCustomerAddressId());
            } else {
                $customerAddress = $order->getShippingAddress();
                $this->log(__METHOD__, "shipping address nuovo - customerOrderId: " . $customerId);
            }

            if ($customerAddress && $customerAddress->getIdIndirizzoCrm()) {
                $idCrmIndirizzo = $customerAddress->getIdIndirizzoCrm();
            } else {
                $idCrmIndirizzo = $this->exportIndirizzo($customerAddress, $customer->getIdAziendaCrm());
                $customerAddress->setIdIndirizzoCrm($idCrmIndirizzo);
            }
        }

        $xmlr = Mage::getModel('bitbull_crm/convert_order')->xmlOrder($order, $idCrmAzienda, $idCrmIndirizzo);
        $params = new stdClass();
        $params->pStrXml = str_replace("<?xml version=\"1.0\"?>\n", "", $xmlr->asXML());
        $this->log(__METHOD__, "$params->pStrXml");
        $client = new SoapClient($this->getExportUrl(), array('trace' => 1));
        $response = $client->InserisciOrdine($params);
        $this->log(__METHOD__, " risposta : " . $client->__getLastResponse());

        $result = $response->InserisciOrdineResult;
        $this->log(__METHOD__, "result ".$result." fine generazione xml ordine");

        if ($result) return (int)$result;

        return false;

    }

    public function exportRichiestacatalogo(array $params)
    {
        try {

            $this->log(__METHOD__, "generazione xml richiedi catalogo");
            $xmlr = Mage::getModel('bitbull_crm/convert_tipocatalogo')->xmlTipocatalogo($params);
            $params = new stdClass();
            $params->pStrXml = str_replace("<?xml version=\"1.0\"?>\n", "", $xmlr->asXML());
            $this->log(__METHOD__, "$params->pStrXml");
            $client = new SoapClient($this->getExportUrl(), array('trace' => 1));
            $result = $client->__soapCall("RichiestaCatalogo", array($params), null,
                new SoapHeader('http://www.eurofides.com/magento', 'mag'));
            $this->log(__METHOD__, " risposta : " . $client->__getLastResponse());

            return $result;

        } catch (Exception $e) {
            $this->log(__METHOD__, "Si è verificato un problema con l'invio della richiesta", Zend_Log::ERR);
            Mage::logException($e);

            throw new Mage_Exception("Si è verificato un problema con l'invio della richiesta.La invitiamo a contattarci ");
        }
    }


    public function sonoAttivo()
    {
        try {
            $this->log(__METHOD__, "verifica che sia operativo il servizio...");
            $xmlr = new SimpleXMLElement("<SonoAttivo></SonoAttivo>");
            $params = new stdClass();
            $params->pStrXml = str_replace("<?xml version=\"1.0\"?>\n", "", $xmlr->asXML());
            $this->log(__METHOD__, "$params->pStrXml");
            $client = new SoapClient($this->getExportUrl(), array('trace' => 1));
            $result = $client->__soapCall("SonoAttivo", array($params), null,
                new SoapHeader('http://www.eurofides.com/magento', 'mag'));
            $this->log(__METHOD__, " risposta : " . $client->__getLastResponse());

            if(is_object($result) && property_exists($result,'SonoAttivoResult') &&  $result->SonoAttivoResult){
                $this->log(__METHOD__, " servizio CRM attivo");

                // Solo in questo caso posso tornare true, in tutti gli altri c'è qualche problema
                return true;
            }
            $this->log(__METHOD__, " errore servizio CRM : " . $client->__getLastResponse());
        } catch (Exception $e) {
            $this->log(__METHOD__, " errore servizio CRM : " . $e->getMessage());

            Mage::logException($e);
        }

        return false;
    }
}