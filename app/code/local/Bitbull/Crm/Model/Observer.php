<?php

/**
 * Created by PhpStorm.
 * User: marco
 * Date: 30/10/15
 * Time: 15.26
 * To change this template use File | Settings | File Templates.
 */
class Bitbull_Crm_Model_Observer extends Mage_Core_Model_Observer
{
    public function insertScheduleOrder($observer)
    {
        try {
            $order = $observer->getOrder();
            Mage::helper('bitbull_crm/scheduler')->schedulingOrder($order);
        } catch (Exception $e) {
            Mage::logException($e);

            // @todo E' assurdo lanciare l'eccezione qui, dal momento che il metodo viene chiamato al sales_order_place_after
            //Mage::throwException($e->getMessage());
        }
    }

    public function exportOrdersCrm($observer)
    {
        try {
            $connectionCrm = Mage::getModel('bitbull_crm/connection');
            $ordersToExport = Mage::getResourceModel('bitbull_crm/scheduled_collection')->addFieldToFilter('status', array('pending'));
            foreach ($ordersToExport as $orderToExport) {
                $order = Mage::getModel('sales/order')->load($orderToExport->getOrderId());

                if ($order->getStatus() == "pending" ||
                    $order->getStatus() == "processing" ||
                    $order->getStatus() == "payment_review" ||
                    ($order->getStatus() == "pending_payment" &&
                        (Mage::getModel('core/date')->timestamp(time()) - Mage::getModel('core/date')->timestamp($order->getCreatedAt())) > 3599
                    )

                ) {
                    //Mage::helper('bitbull_crm')->log(__METHOD__,"status ".$order->getStatus()." - now ".Mage::getModel('core/date')->timestamp(time())." - ".strtotime($order->getCreatedAt())." rightone - ".Mage::getModel('core/date')->timestamp($order->getCreatedAt()) ,Zend_Log::INFO);
                    $result = $connectionCrm->exportOrdine($order, $orderToExport->getCrmAziendaId(), $orderToExport->getCrmIndirizzoId());
                    if (!$result) {
                        Mage::helper('bitbull_crm')->log(__METHOD__, "Si è verificato un problema con l'ordine", Zend_Log::ERR);
                        throw new Mage_Exception("Si è verificato un problema con l'ordine. La invitiamo a contattarci ");
                    } else {
                        $orderToExport->setStatus('exported');
                        $orderToExport->setExecutedAt(strftime('%Y-%m-%d %H:%M:%S', time()));
                        $orderToExport->setErrorReporting($result);
                        $orderToExport->save();
                    }

                }
                unset($order);
            }
        } catch (Exception $e) {
            Mage::logException($e);
            if (isset($order)) {

                if($e->getMessage() != "Bad Request" && strpos($e->getMessage(),'SOAP-ERROR: Parsing WSDL: Couldn\'t load from') === false){
                    $orderToExport->setStatus('error');
                    Mage::helper('bitbull_crm')->sendNotification($order->getRealOrderId(),$e->getMessage(),'exportorders_crm_fatalerror_email_template');
                }else{
                    Mage::helper('bitbull_crm')->sendNotification($order->getRealOrderId(),$e->getMessage());
                }
                $orderToExport->setExecutedAt(strftime('%Y-%m-%d %H:%M:%S', time()));
                $orderToExport->setErrorReporting($e->getMessage());
                $orderToExport->save();
            }
            Mage::throwException($e->getMessage());
        }
    }
}