<?php
class Bitbull_GoogleMerchants_Model_Observer
{
    /**
     * Customise Google Shopping Feed
     *
     * Listens to:
     * - google_shopping_feed_product_data
     *
     * @param Varien_Event_Observer $observer Observer
     */
    public function shoppingFeedProductData(Varien_Event_Observer $observer)
    {
        /* @var $feed Mzentrale_GoogleMerchants_Model_Shopping_Feed */
        $feed = $observer->getFeed();

        /* @var $productData Varien_Object */
        $productData = $observer->getProductData();
        $parent = $observer->getParent();

        if ($feed && $productData) {
            /* @var $product Mage_Catalog_Model_Product */
            $product = $observer->getProduct();
            if ($product) {
                $productData->setData('g:gtin', $feed->getAttributeValue($product, 'eurofides_barcode'));
                $productData->setData('g:brand', 'Eurofides');

                $productData->setData('g:shipping_weight', $feed->getAttributeValue($product, 'weight'));
                $productData->setData('g:price', Mage::helper('core')->currency(((float) $feed->getAttributeValue($product, Bitbull_EurofidesCore_Helper_Data::PRODUCT_ATTRIBUTE_STICKER_UNIT_PRICE))*1.22, false, false)." ".Mage::app()->getStore()->getCurrentCurrencyCode());
                $productData->setData('g:sale_price', null);
                $productData->setData('g:shipping',  array('g:country' => 'IT','g:service'=> 'Corriere Espresso','g:price' => '10.30 EUR'));

            }
            if($parent){
                $productData->setData('title', $parent->getData('eurofides_raggruppamento_desc'));
                $productData->setData('item_group_id', $parent->getData('eurofides_crmid'));
                $productData->setData('g:product_type',  $parent->getData('eurofides_google_tipo'));
                $productData->setData('g:google_product_category',  $parent->getData('eurofides_google_categoria'));
            }
        }
    }

    /**
     * Get allowed attributes
     *
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    public function getAllowAttributes(Mage_Catalog_Model_Product $product)
    {
        return $product->getTypeInstance(true)->getConfigurableAttributes($product);
    }
}
