<?php 
/**
 * ChangeVat view in Frontend Magento
 * 
 * @author: Cantagallo Sandro
 * @version: 1.0
 * @tutorial: use url: changevat.php?vat=inc&page=PRODUCT_SITE_URL to set in user session "show price VAT included" configuration
 * 
 */
require_once 'app/Mage.php';
Mage::app('eurofides_it');

if (isset($_GET['vat']) && isset($_GET['page'])){

	$vat = $_GET['vat']; $page = $_GET['page'];
	if($vat == "excl"){
		$display = "1";
	} elseif ($vat == "inc") {
		$display = "2";
	} elseif ($vat == "both") {
		$display = "3";
	} else {
		$display = "error";
	}

	Mage::getSingleton('core/session', array('name' => 'frontend'));

	if    ($display!="error") {
		Mage::getSingleton('core/session')->setTemporaryTaxDisplay($display);
	}

	//$myData = Mage::getSingleton('core/session')->getTemporaryTaxDisplay();

	header("Location: $page");
}
//Unsetting a session variable: //Mage::getSingleton('core/session')->setMySessionVariable(); //} header("Location: $page"); }


?>